# Sentry 10 On-Premise

Based on the [official sentry docker-compose project](https://github.com/getsentry/onpremise).

Modified to be used with the nginx_reverse proxy.



## Setup

### First time setup

Do *NOT* execute install.sh, it is only there for reference and to avoid modifying the original repo more than necessary. 

1. create you own config files based on sentry.env.template and from the provided templates in sentry/

2. uncomment SSL/TLS options in sentry/sentry.conf.py

3. run `sudo docker-compose run --rm snuba-api bootstrap --force` to bootstrap snuba

4. verify the snuba setup with `sudo docker-compose run --rm clickhouse clickhouse-client -h clickhouse --query="SHOW TABLES;"`: there should be "sentry_local" in the results. Run the previous step until it does.

5. run `sudo docker-compose run --rm web upgrade` and create the sentry superuser when prompted

6. run `sudo docker-compose down` to stop all containers



### Upgrading the installation

TODO: write this

## Running it

Run `sudo docker-compose up`



## Troubleshooting

### Kafka "... failed to change state for partition ... from OfflinePartition to OnlinePartition"

Remove all named volumes with `sudo docker-compose down --volumes` (not sure if this is required, feedback welcome!) and delete these two folders:
- /srv/sentry/kafka
- /srv/sentry/zookeeper

---

original readme:

## Requirements

 * Docker 17.05.0+
 * Compose 1.19.0+

## Minimum Hardware Requirements:

 * You need at least 2400MB RAM

## Setup

To get started with all the defaults, simply clone the repo and run `./install.sh` in your local check-out.

There may need to be modifications to the included example config files (`sentry/config.example.yml` and `sentry/sentry.conf.example.py`) to accommodate your needs or your environment (such as adding GitHub credentials). If you want to perform these, do them before you run the install script and copy them without the `.example` extensions in the name (such as `sentry/sentry.conf.py`) before running the `install.sh` script.

The recommended way to customize your configuration is using the files below, in that order:

 * `config.yml`
 * `sentry.conf.py`
 * `.env` w/ environment variables

We currently support a very minimal set of environment variables to promote other means of configuration.

If you have any issues or questions, our [Community Forum](https://forum.sentry.io/c/on-premise) is at your service! Everytime you run the install script, it will generate a log file, `sentry_install_log-<ISO_TIMESTAMP>.txt` with the output. Sharing these logs would help people diagnose any issues you might be having.

## Event Retention

Sentry comes with a cleanup cron job that prunes events older than `90 days` by default. If you want to change that, you can change the `SENTRY_EVENT_RETENTION_DAYS` environment variable in `.env` or simply override it in your environment. If you do not want the cleanup cron, you can remove the `sentry-cleanup` service from the `docker-compose.yml`file.

## Securing Sentry with SSL/TLS

If you'd like to protect your Sentry install with SSL/TLS, there are
fantastic SSL/TLS proxies like [HAProxy](http://www.haproxy.org/)
and [Nginx](http://nginx.org/). You'll likely want to add this service to your `docker-compose.yml` file.

## Updating Sentry

The included `install.sh` script is meant to be idempotent and to bring you to the latest version. What this means is you can and should run `install.sh` to upgrade to the latest version available. Remember that the output of the script will be stored in a log file, `sentry_install_log-<ISO_TIMESTAMP>.txt`, which you may share for diagnosis if anything goes wrong.

## Resources

 * [Documentation](https://docs.sentry.io/server/)
 * [Bug Tracker](https://github.com/getsentry/onpremise/issues)
 * [Forums](https://forum.sentry.io/c/on-premise)
 * [Discord](https://discord.gg/mg5V76F) (Sentry Community, #sentry-server)


[build-status-image]: https://api.travis-ci.com/getsentry/onpremise.svg?branch=master
[build-status-url]: https://travis-ci.com/getsentry/onpremise
